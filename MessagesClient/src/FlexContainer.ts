import { LitElement, html, css, render } from "lit";
import { customElement, property } from "lit/decorators.js";

@customElement("iy-flex-container")
export default class FlexContainer extends LitElement {

    constructor() {
        super();                          
    }

    static styles = css`

    :host {
        display: var(--flex-container-display, flex);    
        position: var(--flex-container-position, relative);
        flex-direction: var(--flex-container-flex-direction, column);
        border: var(--flex-container-border, 0px solid white);    
    }  
    `;   

    render() {
        
        return html`        
            <slot></slot>                
        `;             

    }    
}