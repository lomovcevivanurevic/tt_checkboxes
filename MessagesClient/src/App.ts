import { LitElement, html, css, render, svg } from "lit";
import { customElement, property, state } from "lit/decorators.js";

@customElement("msg-app")
export class MsgApp extends LitElement { 

  constructor() {
       super();       
  }

  static styles = css`     
        :host {    
                
        }
  `;    

  render() {    

    return html`
          
          <h1>Список сообщений</h1>
          <msg-list></msg-list>
        
    `;
  }
}