import { LitElement, html, css, render } from "lit";
import { customElement, property, state } from "lit/decorators.js";
import { repeat }  from 'lit-html/directives/repeat.js'

@customElement("msg-list")
export class MsgList extends LitElement { 

  @state() _rows: IMessage[] = [];
  
  private _msgsToDelete: number[] = [];

  static styles = css`

        :host {    
          
        }
        
        .cell {
          border: 1px solid gray;
        }       

        .flex-column {
          --flex-container-flex-direction: column;        
        }
        
        .flex-row {
          --flex-container-flex-direction: row;
        }
        
        #table-interface{
          --flex-container-position: fixed;
          bottom: 10;
        }

        #table-container{
          overflow: auto;
        }
  `; 

  constructor() {
       super();

       fetch("/get_messages").then((response) => {        
          response.json().then((data : IMessage[]) => {            
            if (response.status === 200) {
              this._rows = data;
            } else {
                console.error(`Не удалось загрузить список сообщений: ${ response.statusText }`)
                throw new Error(response.statusText);
            }            
          });
       });
  } 
    
  render() {   

      return html`
            
              <div id="table-container">          
                  ${repeat(this._rows, 
                    elem => elem.id,
                    elem => html`
                    <iy-flex-container class="flex-row">
                        <div class="cell"> <input type="checkbox" @change=${ this.__setMsgIdToDelete(elem.id) } > </div>
                        <div class="cell">${ elem.text }</div>                  
                    </iy-flex-container>                     
                  `)}                         
              </div>                              
  
              <iy-flex-container class="flex-row" id="table-interface">
                  <button class="table-del-btn" title="Удалить отмеченные" @click=${ this.__deleteAll }>Удалить отмеченные</button>                
              </iy-flex-container>        
      `;
  }  

  private __deleteAll(event : Event) {   
    
    console.log("Начато удаление");          
    
    fetch("/delete_messages", {
        method: "POST",
        headers: { "Content-Type": "application/json;charset=utf-8" },
        body: JSON.stringify(this._msgsToDelete)
    }).then(response => {
        if (response.status === 200) {

            console.log(`Элементы ${this._msgsToDelete} были успешно удалены!`)                       
            
            const toRemove = new Set(this._msgsToDelete);            

            this._rows = this._rows.filter(x => !toRemove.has(x.id));

            this._msgsToDelete = [];

        } else {
            console.error(`Во время удаления произошла ошибка: ${ response.statusText }`)
            throw new Error(response.statusText);
        }
    })      
  }

  private __setMsgIdToDelete(id: number) {    

    return ( event: Event ) => {
      
      const elem = <HTMLInputElement>event.target;     

      if(elem.checked)
      {
          this._msgsToDelete.push(id);
          console.log(this._msgsToDelete);
      }
      
      else 
      {
          this._msgsToDelete = this._msgsToDelete.filter(x => x !== id);
          console.log(this._msgsToDelete)
      }
    }         
  }
}