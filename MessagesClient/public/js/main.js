var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __decorateClass = (decorators, target, key, kind) => {
  var result = kind > 1 ? void 0 : kind ? __getOwnPropDesc(target, key) : target;
  for (var i5 = decorators.length - 1, decorator; i5 >= 0; i5--)
    if (decorator = decorators[i5])
      result = (kind ? decorator(target, key, result) : decorator(result)) || result;
  if (kind && result)
    __defProp(target, key, result);
  return result;
};

// node_modules/@lit/reactive-element/css-tag.js
var t = window;
var e = t.ShadowRoot && (void 0 === t.ShadyCSS || t.ShadyCSS.nativeShadow) && "adoptedStyleSheets" in Document.prototype && "replace" in CSSStyleSheet.prototype;
var s = Symbol();
var n = /* @__PURE__ */ new WeakMap();
var o = class {
  constructor(t5, e8, n6) {
    if (this._$cssResult$ = true, n6 !== s)
      throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");
    this.cssText = t5, this.t = e8;
  }
  get styleSheet() {
    let t5 = this.o;
    const s6 = this.t;
    if (e && void 0 === t5) {
      const e8 = void 0 !== s6 && 1 === s6.length;
      e8 && (t5 = n.get(s6)), void 0 === t5 && ((this.o = t5 = new CSSStyleSheet()).replaceSync(this.cssText), e8 && n.set(s6, t5));
    }
    return t5;
  }
  toString() {
    return this.cssText;
  }
};
var r = (t5) => new o("string" == typeof t5 ? t5 : t5 + "", void 0, s);
var i = (t5, ...e8) => {
  const n6 = 1 === t5.length ? t5[0] : e8.reduce((e9, s6, n7) => e9 + ((t6) => {
    if (true === t6._$cssResult$)
      return t6.cssText;
    if ("number" == typeof t6)
      return t6;
    throw Error("Value passed to 'css' function must be a 'css' function result: " + t6 + ". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.");
  })(s6) + t5[n7 + 1], t5[0]);
  return new o(n6, t5, s);
};
var S = (s6, n6) => {
  e ? s6.adoptedStyleSheets = n6.map((t5) => t5 instanceof CSSStyleSheet ? t5 : t5.styleSheet) : n6.forEach((e8) => {
    const n7 = document.createElement("style"), o6 = t.litNonce;
    void 0 !== o6 && n7.setAttribute("nonce", o6), n7.textContent = e8.cssText, s6.appendChild(n7);
  });
};
var c = e ? (t5) => t5 : (t5) => t5 instanceof CSSStyleSheet ? ((t6) => {
  let e8 = "";
  for (const s6 of t6.cssRules)
    e8 += s6.cssText;
  return r(e8);
})(t5) : t5;

// node_modules/@lit/reactive-element/reactive-element.js
var s2;
var e2 = window;
var r2 = e2.trustedTypes;
var h = r2 ? r2.emptyScript : "";
var o2 = e2.reactiveElementPolyfillSupport;
var n2 = { toAttribute(t5, i5) {
  switch (i5) {
    case Boolean:
      t5 = t5 ? h : null;
      break;
    case Object:
    case Array:
      t5 = null == t5 ? t5 : JSON.stringify(t5);
  }
  return t5;
}, fromAttribute(t5, i5) {
  let s6 = t5;
  switch (i5) {
    case Boolean:
      s6 = null !== t5;
      break;
    case Number:
      s6 = null === t5 ? null : Number(t5);
      break;
    case Object:
    case Array:
      try {
        s6 = JSON.parse(t5);
      } catch (t6) {
        s6 = null;
      }
  }
  return s6;
} };
var a = (t5, i5) => i5 !== t5 && (i5 == i5 || t5 == t5);
var l = { attribute: true, type: String, converter: n2, reflect: false, hasChanged: a };
var d = class extends HTMLElement {
  constructor() {
    super(), this._$Ei = /* @__PURE__ */ new Map(), this.isUpdatePending = false, this.hasUpdated = false, this._$El = null, this.u();
  }
  static addInitializer(t5) {
    var i5;
    null !== (i5 = this.h) && void 0 !== i5 || (this.h = []), this.h.push(t5);
  }
  static get observedAttributes() {
    this.finalize();
    const t5 = [];
    return this.elementProperties.forEach((i5, s6) => {
      const e8 = this._$Ep(s6, i5);
      void 0 !== e8 && (this._$Ev.set(e8, s6), t5.push(e8));
    }), t5;
  }
  static createProperty(t5, i5 = l) {
    if (i5.state && (i5.attribute = false), this.finalize(), this.elementProperties.set(t5, i5), !i5.noAccessor && !this.prototype.hasOwnProperty(t5)) {
      const s6 = "symbol" == typeof t5 ? Symbol() : "__" + t5, e8 = this.getPropertyDescriptor(t5, s6, i5);
      void 0 !== e8 && Object.defineProperty(this.prototype, t5, e8);
    }
  }
  static getPropertyDescriptor(t5, i5, s6) {
    return { get() {
      return this[i5];
    }, set(e8) {
      const r5 = this[t5];
      this[i5] = e8, this.requestUpdate(t5, r5, s6);
    }, configurable: true, enumerable: true };
  }
  static getPropertyOptions(t5) {
    return this.elementProperties.get(t5) || l;
  }
  static finalize() {
    if (this.hasOwnProperty("finalized"))
      return false;
    this.finalized = true;
    const t5 = Object.getPrototypeOf(this);
    if (t5.finalize(), this.elementProperties = new Map(t5.elementProperties), this._$Ev = /* @__PURE__ */ new Map(), this.hasOwnProperty("properties")) {
      const t6 = this.properties, i5 = [...Object.getOwnPropertyNames(t6), ...Object.getOwnPropertySymbols(t6)];
      for (const s6 of i5)
        this.createProperty(s6, t6[s6]);
    }
    return this.elementStyles = this.finalizeStyles(this.styles), true;
  }
  static finalizeStyles(i5) {
    const s6 = [];
    if (Array.isArray(i5)) {
      const e8 = new Set(i5.flat(1 / 0).reverse());
      for (const i6 of e8)
        s6.unshift(c(i6));
    } else
      void 0 !== i5 && s6.push(c(i5));
    return s6;
  }
  static _$Ep(t5, i5) {
    const s6 = i5.attribute;
    return false === s6 ? void 0 : "string" == typeof s6 ? s6 : "string" == typeof t5 ? t5.toLowerCase() : void 0;
  }
  u() {
    var t5;
    this._$E_ = new Promise((t6) => this.enableUpdating = t6), this._$AL = /* @__PURE__ */ new Map(), this._$Eg(), this.requestUpdate(), null === (t5 = this.constructor.h) || void 0 === t5 || t5.forEach((t6) => t6(this));
  }
  addController(t5) {
    var i5, s6;
    (null !== (i5 = this._$ES) && void 0 !== i5 ? i5 : this._$ES = []).push(t5), void 0 !== this.renderRoot && this.isConnected && (null === (s6 = t5.hostConnected) || void 0 === s6 || s6.call(t5));
  }
  removeController(t5) {
    var i5;
    null === (i5 = this._$ES) || void 0 === i5 || i5.splice(this._$ES.indexOf(t5) >>> 0, 1);
  }
  _$Eg() {
    this.constructor.elementProperties.forEach((t5, i5) => {
      this.hasOwnProperty(i5) && (this._$Ei.set(i5, this[i5]), delete this[i5]);
    });
  }
  createRenderRoot() {
    var t5;
    const s6 = null !== (t5 = this.shadowRoot) && void 0 !== t5 ? t5 : this.attachShadow(this.constructor.shadowRootOptions);
    return S(s6, this.constructor.elementStyles), s6;
  }
  connectedCallback() {
    var t5;
    void 0 === this.renderRoot && (this.renderRoot = this.createRenderRoot()), this.enableUpdating(true), null === (t5 = this._$ES) || void 0 === t5 || t5.forEach((t6) => {
      var i5;
      return null === (i5 = t6.hostConnected) || void 0 === i5 ? void 0 : i5.call(t6);
    });
  }
  enableUpdating(t5) {
  }
  disconnectedCallback() {
    var t5;
    null === (t5 = this._$ES) || void 0 === t5 || t5.forEach((t6) => {
      var i5;
      return null === (i5 = t6.hostDisconnected) || void 0 === i5 ? void 0 : i5.call(t6);
    });
  }
  attributeChangedCallback(t5, i5, s6) {
    this._$AK(t5, s6);
  }
  _$EO(t5, i5, s6 = l) {
    var e8;
    const r5 = this.constructor._$Ep(t5, s6);
    if (void 0 !== r5 && true === s6.reflect) {
      const h3 = (void 0 !== (null === (e8 = s6.converter) || void 0 === e8 ? void 0 : e8.toAttribute) ? s6.converter : n2).toAttribute(i5, s6.type);
      this._$El = t5, null == h3 ? this.removeAttribute(r5) : this.setAttribute(r5, h3), this._$El = null;
    }
  }
  _$AK(t5, i5) {
    var s6;
    const e8 = this.constructor, r5 = e8._$Ev.get(t5);
    if (void 0 !== r5 && this._$El !== r5) {
      const t6 = e8.getPropertyOptions(r5), h3 = "function" == typeof t6.converter ? { fromAttribute: t6.converter } : void 0 !== (null === (s6 = t6.converter) || void 0 === s6 ? void 0 : s6.fromAttribute) ? t6.converter : n2;
      this._$El = r5, this[r5] = h3.fromAttribute(i5, t6.type), this._$El = null;
    }
  }
  requestUpdate(t5, i5, s6) {
    let e8 = true;
    void 0 !== t5 && (((s6 = s6 || this.constructor.getPropertyOptions(t5)).hasChanged || a)(this[t5], i5) ? (this._$AL.has(t5) || this._$AL.set(t5, i5), true === s6.reflect && this._$El !== t5 && (void 0 === this._$EC && (this._$EC = /* @__PURE__ */ new Map()), this._$EC.set(t5, s6))) : e8 = false), !this.isUpdatePending && e8 && (this._$E_ = this._$Ej());
  }
  async _$Ej() {
    this.isUpdatePending = true;
    try {
      await this._$E_;
    } catch (t6) {
      Promise.reject(t6);
    }
    const t5 = this.scheduleUpdate();
    return null != t5 && await t5, !this.isUpdatePending;
  }
  scheduleUpdate() {
    return this.performUpdate();
  }
  performUpdate() {
    var t5;
    if (!this.isUpdatePending)
      return;
    this.hasUpdated, this._$Ei && (this._$Ei.forEach((t6, i6) => this[i6] = t6), this._$Ei = void 0);
    let i5 = false;
    const s6 = this._$AL;
    try {
      i5 = this.shouldUpdate(s6), i5 ? (this.willUpdate(s6), null === (t5 = this._$ES) || void 0 === t5 || t5.forEach((t6) => {
        var i6;
        return null === (i6 = t6.hostUpdate) || void 0 === i6 ? void 0 : i6.call(t6);
      }), this.update(s6)) : this._$Ek();
    } catch (t6) {
      throw i5 = false, this._$Ek(), t6;
    }
    i5 && this._$AE(s6);
  }
  willUpdate(t5) {
  }
  _$AE(t5) {
    var i5;
    null === (i5 = this._$ES) || void 0 === i5 || i5.forEach((t6) => {
      var i6;
      return null === (i6 = t6.hostUpdated) || void 0 === i6 ? void 0 : i6.call(t6);
    }), this.hasUpdated || (this.hasUpdated = true, this.firstUpdated(t5)), this.updated(t5);
  }
  _$Ek() {
    this._$AL = /* @__PURE__ */ new Map(), this.isUpdatePending = false;
  }
  get updateComplete() {
    return this.getUpdateComplete();
  }
  getUpdateComplete() {
    return this._$E_;
  }
  shouldUpdate(t5) {
    return true;
  }
  update(t5) {
    void 0 !== this._$EC && (this._$EC.forEach((t6, i5) => this._$EO(i5, this[i5], t6)), this._$EC = void 0), this._$Ek();
  }
  updated(t5) {
  }
  firstUpdated(t5) {
  }
};
d.finalized = true, d.elementProperties = /* @__PURE__ */ new Map(), d.elementStyles = [], d.shadowRootOptions = { mode: "open" }, null == o2 || o2({ ReactiveElement: d }), (null !== (s2 = e2.reactiveElementVersions) && void 0 !== s2 ? s2 : e2.reactiveElementVersions = []).push("1.4.1");

// node_modules/lit-html/lit-html.js
var t2;
var i2 = window;
var s3 = i2.trustedTypes;
var e3 = s3 ? s3.createPolicy("lit-html", { createHTML: (t5) => t5 }) : void 0;
var o3 = `lit$${(Math.random() + "").slice(9)}$`;
var n3 = "?" + o3;
var l2 = `<${n3}>`;
var h2 = document;
var r3 = (t5 = "") => h2.createComment(t5);
var d2 = (t5) => null === t5 || "object" != typeof t5 && "function" != typeof t5;
var u = Array.isArray;
var c2 = (t5) => u(t5) || "function" == typeof (null == t5 ? void 0 : t5[Symbol.iterator]);
var v = /<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g;
var a2 = /-->/g;
var f = />/g;
var _ = RegExp(`>|[ 	
\f\r](?:([^\\s"'>=/]+)([ 	
\f\r]*=[ 	
\f\r]*(?:[^ 	
\f\r"'\`<>=]|("|')|))|$)`, "g");
var m = /'/g;
var p = /"/g;
var $ = /^(?:script|style|textarea|title)$/i;
var g = (t5) => (i5, ...s6) => ({ _$litType$: t5, strings: i5, values: s6 });
var y = g(1);
var w = g(2);
var x = Symbol.for("lit-noChange");
var b = Symbol.for("lit-nothing");
var T = /* @__PURE__ */ new WeakMap();
var A = (t5, i5, s6) => {
  var e8, o6;
  const n6 = null !== (e8 = null == s6 ? void 0 : s6.renderBefore) && void 0 !== e8 ? e8 : i5;
  let l6 = n6._$litPart$;
  if (void 0 === l6) {
    const t6 = null !== (o6 = null == s6 ? void 0 : s6.renderBefore) && void 0 !== o6 ? o6 : null;
    n6._$litPart$ = l6 = new S2(i5.insertBefore(r3(), t6), t6, void 0, null != s6 ? s6 : {});
  }
  return l6._$AI(t5), l6;
};
var E = h2.createTreeWalker(h2, 129, null, false);
var C = (t5, i5) => {
  const s6 = t5.length - 1, n6 = [];
  let h3, r5 = 2 === i5 ? "<svg>" : "", d3 = v;
  for (let i6 = 0; i6 < s6; i6++) {
    const s7 = t5[i6];
    let e8, u5, c5 = -1, g2 = 0;
    for (; g2 < s7.length && (d3.lastIndex = g2, u5 = d3.exec(s7), null !== u5); )
      g2 = d3.lastIndex, d3 === v ? "!--" === u5[1] ? d3 = a2 : void 0 !== u5[1] ? d3 = f : void 0 !== u5[2] ? ($.test(u5[2]) && (h3 = RegExp("</" + u5[2], "g")), d3 = _) : void 0 !== u5[3] && (d3 = _) : d3 === _ ? ">" === u5[0] ? (d3 = null != h3 ? h3 : v, c5 = -1) : void 0 === u5[1] ? c5 = -2 : (c5 = d3.lastIndex - u5[2].length, e8 = u5[1], d3 = void 0 === u5[3] ? _ : '"' === u5[3] ? p : m) : d3 === p || d3 === m ? d3 = _ : d3 === a2 || d3 === f ? d3 = v : (d3 = _, h3 = void 0);
    const y2 = d3 === _ && t5[i6 + 1].startsWith("/>") ? " " : "";
    r5 += d3 === v ? s7 + l2 : c5 >= 0 ? (n6.push(e8), s7.slice(0, c5) + "$lit$" + s7.slice(c5) + o3 + y2) : s7 + o3 + (-2 === c5 ? (n6.push(void 0), i6) : y2);
  }
  const u4 = r5 + (t5[s6] || "<?>") + (2 === i5 ? "</svg>" : "");
  if (!Array.isArray(t5) || !t5.hasOwnProperty("raw"))
    throw Error("invalid template strings array");
  return [void 0 !== e3 ? e3.createHTML(u4) : u4, n6];
};
var P = class {
  constructor({ strings: t5, _$litType$: i5 }, e8) {
    let l6;
    this.parts = [];
    let h3 = 0, d3 = 0;
    const u4 = t5.length - 1, c5 = this.parts, [v2, a3] = C(t5, i5);
    if (this.el = P.createElement(v2, e8), E.currentNode = this.el.content, 2 === i5) {
      const t6 = this.el.content, i6 = t6.firstChild;
      i6.remove(), t6.append(...i6.childNodes);
    }
    for (; null !== (l6 = E.nextNode()) && c5.length < u4; ) {
      if (1 === l6.nodeType) {
        if (l6.hasAttributes()) {
          const t6 = [];
          for (const i6 of l6.getAttributeNames())
            if (i6.endsWith("$lit$") || i6.startsWith(o3)) {
              const s6 = a3[d3++];
              if (t6.push(i6), void 0 !== s6) {
                const t7 = l6.getAttribute(s6.toLowerCase() + "$lit$").split(o3), i7 = /([.?@])?(.*)/.exec(s6);
                c5.push({ type: 1, index: h3, name: i7[2], strings: t7, ctor: "." === i7[1] ? R : "?" === i7[1] ? H : "@" === i7[1] ? I : M });
              } else
                c5.push({ type: 6, index: h3 });
            }
          for (const i6 of t6)
            l6.removeAttribute(i6);
        }
        if ($.test(l6.tagName)) {
          const t6 = l6.textContent.split(o3), i6 = t6.length - 1;
          if (i6 > 0) {
            l6.textContent = s3 ? s3.emptyScript : "";
            for (let s6 = 0; s6 < i6; s6++)
              l6.append(t6[s6], r3()), E.nextNode(), c5.push({ type: 2, index: ++h3 });
            l6.append(t6[i6], r3());
          }
        }
      } else if (8 === l6.nodeType)
        if (l6.data === n3)
          c5.push({ type: 2, index: h3 });
        else {
          let t6 = -1;
          for (; -1 !== (t6 = l6.data.indexOf(o3, t6 + 1)); )
            c5.push({ type: 7, index: h3 }), t6 += o3.length - 1;
        }
      h3++;
    }
  }
  static createElement(t5, i5) {
    const s6 = h2.createElement("template");
    return s6.innerHTML = t5, s6;
  }
};
function V(t5, i5, s6 = t5, e8) {
  var o6, n6, l6, h3;
  if (i5 === x)
    return i5;
  let r5 = void 0 !== e8 ? null === (o6 = s6._$Cl) || void 0 === o6 ? void 0 : o6[e8] : s6._$Cu;
  const u4 = d2(i5) ? void 0 : i5._$litDirective$;
  return (null == r5 ? void 0 : r5.constructor) !== u4 && (null === (n6 = null == r5 ? void 0 : r5._$AO) || void 0 === n6 || n6.call(r5, false), void 0 === u4 ? r5 = void 0 : (r5 = new u4(t5), r5._$AT(t5, s6, e8)), void 0 !== e8 ? (null !== (l6 = (h3 = s6)._$Cl) && void 0 !== l6 ? l6 : h3._$Cl = [])[e8] = r5 : s6._$Cu = r5), void 0 !== r5 && (i5 = V(t5, r5._$AS(t5, i5.values), r5, e8)), i5;
}
var N = class {
  constructor(t5, i5) {
    this.v = [], this._$AN = void 0, this._$AD = t5, this._$AM = i5;
  }
  get parentNode() {
    return this._$AM.parentNode;
  }
  get _$AU() {
    return this._$AM._$AU;
  }
  p(t5) {
    var i5;
    const { el: { content: s6 }, parts: e8 } = this._$AD, o6 = (null !== (i5 = null == t5 ? void 0 : t5.creationScope) && void 0 !== i5 ? i5 : h2).importNode(s6, true);
    E.currentNode = o6;
    let n6 = E.nextNode(), l6 = 0, r5 = 0, d3 = e8[0];
    for (; void 0 !== d3; ) {
      if (l6 === d3.index) {
        let i6;
        2 === d3.type ? i6 = new S2(n6, n6.nextSibling, this, t5) : 1 === d3.type ? i6 = new d3.ctor(n6, d3.name, d3.strings, this, t5) : 6 === d3.type && (i6 = new L(n6, this, t5)), this.v.push(i6), d3 = e8[++r5];
      }
      l6 !== (null == d3 ? void 0 : d3.index) && (n6 = E.nextNode(), l6++);
    }
    return o6;
  }
  m(t5) {
    let i5 = 0;
    for (const s6 of this.v)
      void 0 !== s6 && (void 0 !== s6.strings ? (s6._$AI(t5, s6, i5), i5 += s6.strings.length - 2) : s6._$AI(t5[i5])), i5++;
  }
};
var S2 = class {
  constructor(t5, i5, s6, e8) {
    var o6;
    this.type = 2, this._$AH = b, this._$AN = void 0, this._$AA = t5, this._$AB = i5, this._$AM = s6, this.options = e8, this._$C_ = null === (o6 = null == e8 ? void 0 : e8.isConnected) || void 0 === o6 || o6;
  }
  get _$AU() {
    var t5, i5;
    return null !== (i5 = null === (t5 = this._$AM) || void 0 === t5 ? void 0 : t5._$AU) && void 0 !== i5 ? i5 : this._$C_;
  }
  get parentNode() {
    let t5 = this._$AA.parentNode;
    const i5 = this._$AM;
    return void 0 !== i5 && 11 === t5.nodeType && (t5 = i5.parentNode), t5;
  }
  get startNode() {
    return this._$AA;
  }
  get endNode() {
    return this._$AB;
  }
  _$AI(t5, i5 = this) {
    t5 = V(this, t5, i5), d2(t5) ? t5 === b || null == t5 || "" === t5 ? (this._$AH !== b && this._$AR(), this._$AH = b) : t5 !== this._$AH && t5 !== x && this.$(t5) : void 0 !== t5._$litType$ ? this.T(t5) : void 0 !== t5.nodeType ? this.k(t5) : c2(t5) ? this.O(t5) : this.$(t5);
  }
  S(t5, i5 = this._$AB) {
    return this._$AA.parentNode.insertBefore(t5, i5);
  }
  k(t5) {
    this._$AH !== t5 && (this._$AR(), this._$AH = this.S(t5));
  }
  $(t5) {
    this._$AH !== b && d2(this._$AH) ? this._$AA.nextSibling.data = t5 : this.k(h2.createTextNode(t5)), this._$AH = t5;
  }
  T(t5) {
    var i5;
    const { values: s6, _$litType$: e8 } = t5, o6 = "number" == typeof e8 ? this._$AC(t5) : (void 0 === e8.el && (e8.el = P.createElement(e8.h, this.options)), e8);
    if ((null === (i5 = this._$AH) || void 0 === i5 ? void 0 : i5._$AD) === o6)
      this._$AH.m(s6);
    else {
      const t6 = new N(o6, this), i6 = t6.p(this.options);
      t6.m(s6), this.k(i6), this._$AH = t6;
    }
  }
  _$AC(t5) {
    let i5 = T.get(t5.strings);
    return void 0 === i5 && T.set(t5.strings, i5 = new P(t5)), i5;
  }
  O(t5) {
    u(this._$AH) || (this._$AH = [], this._$AR());
    const i5 = this._$AH;
    let s6, e8 = 0;
    for (const o6 of t5)
      e8 === i5.length ? i5.push(s6 = new S2(this.S(r3()), this.S(r3()), this, this.options)) : s6 = i5[e8], s6._$AI(o6), e8++;
    e8 < i5.length && (this._$AR(s6 && s6._$AB.nextSibling, e8), i5.length = e8);
  }
  _$AR(t5 = this._$AA.nextSibling, i5) {
    var s6;
    for (null === (s6 = this._$AP) || void 0 === s6 || s6.call(this, false, true, i5); t5 && t5 !== this._$AB; ) {
      const i6 = t5.nextSibling;
      t5.remove(), t5 = i6;
    }
  }
  setConnected(t5) {
    var i5;
    void 0 === this._$AM && (this._$C_ = t5, null === (i5 = this._$AP) || void 0 === i5 || i5.call(this, t5));
  }
};
var M = class {
  constructor(t5, i5, s6, e8, o6) {
    this.type = 1, this._$AH = b, this._$AN = void 0, this.element = t5, this.name = i5, this._$AM = e8, this.options = o6, s6.length > 2 || "" !== s6[0] || "" !== s6[1] ? (this._$AH = Array(s6.length - 1).fill(new String()), this.strings = s6) : this._$AH = b;
  }
  get tagName() {
    return this.element.tagName;
  }
  get _$AU() {
    return this._$AM._$AU;
  }
  _$AI(t5, i5 = this, s6, e8) {
    const o6 = this.strings;
    let n6 = false;
    if (void 0 === o6)
      t5 = V(this, t5, i5, 0), n6 = !d2(t5) || t5 !== this._$AH && t5 !== x, n6 && (this._$AH = t5);
    else {
      const e9 = t5;
      let l6, h3;
      for (t5 = o6[0], l6 = 0; l6 < o6.length - 1; l6++)
        h3 = V(this, e9[s6 + l6], i5, l6), h3 === x && (h3 = this._$AH[l6]), n6 || (n6 = !d2(h3) || h3 !== this._$AH[l6]), h3 === b ? t5 = b : t5 !== b && (t5 += (null != h3 ? h3 : "") + o6[l6 + 1]), this._$AH[l6] = h3;
    }
    n6 && !e8 && this.P(t5);
  }
  P(t5) {
    t5 === b ? this.element.removeAttribute(this.name) : this.element.setAttribute(this.name, null != t5 ? t5 : "");
  }
};
var R = class extends M {
  constructor() {
    super(...arguments), this.type = 3;
  }
  P(t5) {
    this.element[this.name] = t5 === b ? void 0 : t5;
  }
};
var k = s3 ? s3.emptyScript : "";
var H = class extends M {
  constructor() {
    super(...arguments), this.type = 4;
  }
  P(t5) {
    t5 && t5 !== b ? this.element.setAttribute(this.name, k) : this.element.removeAttribute(this.name);
  }
};
var I = class extends M {
  constructor(t5, i5, s6, e8, o6) {
    super(t5, i5, s6, e8, o6), this.type = 5;
  }
  _$AI(t5, i5 = this) {
    var s6;
    if ((t5 = null !== (s6 = V(this, t5, i5, 0)) && void 0 !== s6 ? s6 : b) === x)
      return;
    const e8 = this._$AH, o6 = t5 === b && e8 !== b || t5.capture !== e8.capture || t5.once !== e8.once || t5.passive !== e8.passive, n6 = t5 !== b && (e8 === b || o6);
    o6 && this.element.removeEventListener(this.name, this, e8), n6 && this.element.addEventListener(this.name, this, t5), this._$AH = t5;
  }
  handleEvent(t5) {
    var i5, s6;
    "function" == typeof this._$AH ? this._$AH.call(null !== (s6 = null === (i5 = this.options) || void 0 === i5 ? void 0 : i5.host) && void 0 !== s6 ? s6 : this.element, t5) : this._$AH.handleEvent(t5);
  }
};
var L = class {
  constructor(t5, i5, s6) {
    this.element = t5, this.type = 6, this._$AN = void 0, this._$AM = i5, this.options = s6;
  }
  get _$AU() {
    return this._$AM._$AU;
  }
  _$AI(t5) {
    V(this, t5);
  }
};
var z = { A: "$lit$", M: o3, C: n3, L: 1, R: C, D: N, V: c2, I: V, H: S2, N: M, U: H, B: I, F: R, W: L };
var Z = i2.litHtmlPolyfillSupport;
null == Z || Z(P, S2), (null !== (t2 = i2.litHtmlVersions) && void 0 !== t2 ? t2 : i2.litHtmlVersions = []).push("2.3.1");

// node_modules/lit-element/lit-element.js
var l3;
var o4;
var s4 = class extends d {
  constructor() {
    super(...arguments), this.renderOptions = { host: this }, this._$Do = void 0;
  }
  createRenderRoot() {
    var t5, e8;
    const i5 = super.createRenderRoot();
    return null !== (t5 = (e8 = this.renderOptions).renderBefore) && void 0 !== t5 || (e8.renderBefore = i5.firstChild), i5;
  }
  update(t5) {
    const i5 = this.render();
    this.hasUpdated || (this.renderOptions.isConnected = this.isConnected), super.update(t5), this._$Do = A(i5, this.renderRoot, this.renderOptions);
  }
  connectedCallback() {
    var t5;
    super.connectedCallback(), null === (t5 = this._$Do) || void 0 === t5 || t5.setConnected(true);
  }
  disconnectedCallback() {
    var t5;
    super.disconnectedCallback(), null === (t5 = this._$Do) || void 0 === t5 || t5.setConnected(false);
  }
  render() {
    return x;
  }
};
s4.finalized = true, s4._$litElement$ = true, null === (l3 = globalThis.litElementHydrateSupport) || void 0 === l3 || l3.call(globalThis, { LitElement: s4 });
var n4 = globalThis.litElementPolyfillSupport;
null == n4 || n4({ LitElement: s4 });
(null !== (o4 = globalThis.litElementVersions) && void 0 !== o4 ? o4 : globalThis.litElementVersions = []).push("3.2.2");

// node_modules/@lit/reactive-element/decorators/custom-element.js
var e4 = (e8) => (n6) => "function" == typeof n6 ? ((e9, n7) => (customElements.define(e9, n7), n7))(e8, n6) : ((e9, n7) => {
  const { kind: t5, elements: s6 } = n7;
  return { kind: t5, elements: s6, finisher(n8) {
    customElements.define(e9, n8);
  } };
})(e8, n6);

// node_modules/@lit/reactive-element/decorators/property.js
var i3 = (i5, e8) => "method" === e8.kind && e8.descriptor && !("value" in e8.descriptor) ? { ...e8, finisher(n6) {
  n6.createProperty(e8.key, i5);
} } : { kind: "field", key: Symbol(), placement: "own", descriptor: {}, originalKey: e8.key, initializer() {
  "function" == typeof e8.initializer && (this[e8.key] = e8.initializer.call(this));
}, finisher(n6) {
  n6.createProperty(e8.key, i5);
} };
function e5(e8) {
  return (n6, t5) => void 0 !== t5 ? ((i5, e9, n7) => {
    e9.constructor.createProperty(n7, i5);
  })(e8, n6, t5) : i3(e8, n6);
}

// node_modules/@lit/reactive-element/decorators/state.js
function t3(t5) {
  return e5({ ...t5, state: true });
}

// node_modules/@lit/reactive-element/decorators/query-assigned-elements.js
var n5;
var e6 = null != (null === (n5 = window.HTMLSlotElement) || void 0 === n5 ? void 0 : n5.prototype.assignedElements) ? (o6, n6) => o6.assignedElements(n6) : (o6, n6) => o6.assignedNodes(n6).filter((o7) => o7.nodeType === Node.ELEMENT_NODE);

// src/FlexContainer.ts
var FlexContainer = class extends s4 {
  constructor() {
    super();
  }
  render() {
    return y`        
            <slot></slot>                
        `;
  }
};
FlexContainer.styles = i`

    :host {
        display: var(--flex-container-display, flex);    
        position: var(--flex-container-position, relative);
        flex-direction: var(--flex-container-flex-direction, column);
        border: var(--flex-container-border, 0px solid white);    
    }  
    `;
FlexContainer = __decorateClass([
  e4("iy-flex-container")
], FlexContainer);

// node_modules/lit-html/directive.js
var t4 = { ATTRIBUTE: 1, CHILD: 2, PROPERTY: 3, BOOLEAN_ATTRIBUTE: 4, EVENT: 5, ELEMENT: 6 };
var e7 = (t5) => (...e8) => ({ _$litDirective$: t5, values: e8 });
var i4 = class {
  constructor(t5) {
  }
  get _$AU() {
    return this._$AM._$AU;
  }
  _$AT(t5, e8, i5) {
    this._$Ct = t5, this._$AM = e8, this._$Ci = i5;
  }
  _$AS(t5, e8) {
    return this.update(t5, e8);
  }
  update(t5, e8) {
    return this.render(...e8);
  }
};

// node_modules/lit-html/directive-helpers.js
var { H: l5 } = z;
var c3 = () => document.createComment("");
var r4 = (o6, t5, i5) => {
  var n6;
  const d3 = o6._$AA.parentNode, v2 = void 0 === t5 ? o6._$AB : t5._$AA;
  if (void 0 === i5) {
    const t6 = d3.insertBefore(c3(), v2), n7 = d3.insertBefore(c3(), v2);
    i5 = new l5(t6, n7, o6, o6.options);
  } else {
    const l6 = i5._$AB.nextSibling, t6 = i5._$AM, e8 = t6 !== o6;
    if (e8) {
      let l7;
      null === (n6 = i5._$AQ) || void 0 === n6 || n6.call(i5, o6), i5._$AM = o6, void 0 !== i5._$AP && (l7 = o6._$AU) !== t6._$AU && i5._$AP(l7);
    }
    if (l6 !== v2 || e8) {
      let o7 = i5._$AA;
      for (; o7 !== l6; ) {
        const l7 = o7.nextSibling;
        d3.insertBefore(o7, v2), o7 = l7;
      }
    }
  }
  return i5;
};
var u2 = (o6, l6, t5 = o6) => (o6._$AI(l6, t5), o6);
var f2 = {};
var s5 = (o6, l6 = f2) => o6._$AH = l6;
var m2 = (o6) => o6._$AH;
var p2 = (o6) => {
  var l6;
  null === (l6 = o6._$AP) || void 0 === l6 || l6.call(o6, false, true);
  let t5 = o6._$AA;
  const i5 = o6._$AB.nextSibling;
  for (; t5 !== i5; ) {
    const o7 = t5.nextSibling;
    t5.remove(), t5 = o7;
  }
};

// node_modules/lit-html/directives/repeat.js
var u3 = (e8, s6, t5) => {
  const r5 = /* @__PURE__ */ new Map();
  for (let l6 = s6; l6 <= t5; l6++)
    r5.set(e8[l6], l6);
  return r5;
};
var c4 = e7(class extends i4 {
  constructor(e8) {
    if (super(e8), e8.type !== t4.CHILD)
      throw Error("repeat() can only be used in text expressions");
  }
  ht(e8, s6, t5) {
    let r5;
    void 0 === t5 ? t5 = s6 : void 0 !== s6 && (r5 = s6);
    const l6 = [], o6 = [];
    let i5 = 0;
    for (const s7 of e8)
      l6[i5] = r5 ? r5(s7, i5) : i5, o6[i5] = t5(s7, i5), i5++;
    return { values: o6, keys: l6 };
  }
  render(e8, s6, t5) {
    return this.ht(e8, s6, t5).values;
  }
  update(s6, [t5, r5, c5]) {
    var d3;
    const a3 = m2(s6), { values: p3, keys: v2 } = this.ht(t5, r5, c5);
    if (!Array.isArray(a3))
      return this.ut = v2, p3;
    const h3 = null !== (d3 = this.ut) && void 0 !== d3 ? d3 : this.ut = [], m3 = [];
    let y2, x2, j = 0, k2 = a3.length - 1, w2 = 0, A2 = p3.length - 1;
    for (; j <= k2 && w2 <= A2; )
      if (null === a3[j])
        j++;
      else if (null === a3[k2])
        k2--;
      else if (h3[j] === v2[w2])
        m3[w2] = u2(a3[j], p3[w2]), j++, w2++;
      else if (h3[k2] === v2[A2])
        m3[A2] = u2(a3[k2], p3[A2]), k2--, A2--;
      else if (h3[j] === v2[A2])
        m3[A2] = u2(a3[j], p3[A2]), r4(s6, m3[A2 + 1], a3[j]), j++, A2--;
      else if (h3[k2] === v2[w2])
        m3[w2] = u2(a3[k2], p3[w2]), r4(s6, a3[j], a3[k2]), k2--, w2++;
      else if (void 0 === y2 && (y2 = u3(v2, w2, A2), x2 = u3(h3, j, k2)), y2.has(h3[j]))
        if (y2.has(h3[k2])) {
          const e8 = x2.get(v2[w2]), t6 = void 0 !== e8 ? a3[e8] : null;
          if (null === t6) {
            const e9 = r4(s6, a3[j]);
            u2(e9, p3[w2]), m3[w2] = e9;
          } else
            m3[w2] = u2(t6, p3[w2]), r4(s6, a3[j], t6), a3[e8] = null;
          w2++;
        } else
          p2(a3[k2]), k2--;
      else
        p2(a3[j]), j++;
    for (; w2 <= A2; ) {
      const e8 = r4(s6, m3[A2 + 1]);
      u2(e8, p3[w2]), m3[w2++] = e8;
    }
    for (; j <= k2; ) {
      const e8 = a3[j++];
      null !== e8 && p2(e8);
    }
    return this.ut = v2, s5(s6, m3), x;
  }
});

// src/MessagesList.ts
var MsgList = class extends s4 {
  constructor() {
    super();
    this._rows = [];
    this._msgsToDelete = [];
    fetch("/get_messages").then((response) => {
      response.json().then((data) => {
        if (response.status === 200) {
          this._rows = data;
        } else {
          console.error(`\u041D\u0435 \u0443\u0434\u0430\u043B\u043E\u0441\u044C \u0437\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044C \u0441\u043F\u0438\u0441\u043E\u043A \u0441\u043E\u043E\u0431\u0449\u0435\u043D\u0438\u0439: ${response.statusText}`);
          throw new Error(response.statusText);
        }
      });
    });
  }
  render() {
    return y`
            
              <div id="table-container">          
                  ${c4(
      this._rows,
      (elem) => elem.id,
      (elem) => y`
                    <iy-flex-container class="flex-row">
                        <div class="cell"> <input type="checkbox" @change=${this.__setMsgIdToDelete(elem.id)} > </div>
                        <div class="cell">${elem.text}</div>                  
                    </iy-flex-container>                     
                  `
    )}                         
              </div>                              
  
              <iy-flex-container class="flex-row" id="table-interface">
                  <button class="table-del-btn" title="Удалить отмеченные" @click=${this.__deleteAll}>Удалить отмеченные</button>                
              </iy-flex-container>        
      `;
  }
  __deleteAll(event) {
    console.log("\u041D\u0430\u0447\u0430\u0442\u043E \u0443\u0434\u0430\u043B\u0435\u043D\u0438\u0435");
    fetch("/delete_messages", {
      method: "POST",
      headers: { "Content-Type": "application/json;charset=utf-8" },
      body: JSON.stringify(this._msgsToDelete)
    }).then((response) => {
      if (response.status === 200) {
        console.log(`\u042D\u043B\u0435\u043C\u0435\u043D\u0442\u044B ${this._msgsToDelete} \u0431\u044B\u043B\u0438 \u0443\u0441\u043F\u0435\u0448\u043D\u043E \u0443\u0434\u0430\u043B\u0435\u043D\u044B!`);
        const toRemove = new Set(this._msgsToDelete);
        this._rows = this._rows.filter((x2) => !toRemove.has(x2.id));
        this._msgsToDelete = [];
      } else {
        console.error(`\u0412\u043E \u0432\u0440\u0435\u043C\u044F \u0443\u0434\u0430\u043B\u0435\u043D\u0438\u044F \u043F\u0440\u043E\u0438\u0437\u043E\u0448\u043B\u0430 \u043E\u0448\u0438\u0431\u043A\u0430: ${response.statusText}`);
        throw new Error(response.statusText);
      }
    });
  }
  __setMsgIdToDelete(id) {
    return (event) => {
      const elem = event.target;
      if (elem.checked) {
        this._msgsToDelete.push(id);
        console.log(this._msgsToDelete);
      } else {
        this._msgsToDelete = this._msgsToDelete.filter((x2) => x2 !== id);
        console.log(this._msgsToDelete);
      }
    };
  }
};
MsgList.styles = i`

        :host {    
          
        }
        
        .cell {
          border: 1px solid gray;
        }       

        .flex-column {
          --flex-container-flex-direction: column;        
        }
        
        .flex-row {
          --flex-container-flex-direction: row;
        }
        
        #table-interface{
          --flex-container-position: fixed;
          bottom: 10;
        }

        #table-container{
          overflow: auto;
        }
  `;
__decorateClass([
  t3()
], MsgList.prototype, "_rows", 2);
MsgList = __decorateClass([
  e4("msg-list")
], MsgList);

// src/App.ts
var MsgApp = class extends s4 {
  constructor() {
    super();
  }
  render() {
    return y`
          
          <h1>Список сообщений</h1>
          <msg-list></msg-list>
        
    `;
  }
};
MsgApp.styles = i`     
        :host {    
                
        }
  `;
MsgApp = __decorateClass([
  e4("msg-app")
], MsgApp);
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
/**
 * @license
 * Copyright 2020 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
//# sourceMappingURL=main.js.map
