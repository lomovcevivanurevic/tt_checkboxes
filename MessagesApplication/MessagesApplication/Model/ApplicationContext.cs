﻿namespace MessagesApplication.Model
{
    public class ApplicationContext : DbContext
    {       
        public DbSet<Message> Messages => Set<Message>();
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
