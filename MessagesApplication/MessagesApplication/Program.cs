using MessagesApplication.Model;

var builder = WebApplication.CreateBuilder(
    new WebApplicationOptions { WebRootPath = "../../MessagesClient/public" });

string connection = builder.Configuration.GetConnectionString("Sqlite");

builder.Services.AddDbContext<ApplicationContext>(options => options.UseSqlite(connection));

var app = builder.Build();

app.UseHttpsRedirection();

app.UseStaticFiles();

app.MapGet("/get_messages", async (ApplicationContext db) => await db.Messages.ToListAsync());

app.MapGet("/get_message" + "/{id}", async (int id, ApplicationContext db) =>
    await db.Messages.FindAsync(id)
        is Message m
            ? Results.Ok(m)
            : Results.NotFound());


app.MapPost("/add_message", async (Message m, ApplicationContext db) =>
{
    db.Messages.Add(m);
    await db.SaveChangesAsync();    

    return Results.Created($"/add_message/{m.Id}", m);
});

app.MapPut("/update_message", async (Message inputMsg, ApplicationContext db) =>
{
    var msg = await db.Messages.FirstOrDefaultAsync(m => m.Id == inputMsg.Id);

    if (msg is null) return Results.NotFound();

    msg.Text = inputMsg.Text;    

    await db.SaveChangesAsync();

    return Results.Ok(msg);
});

app.MapDelete("delete_message" + "/{id}", async (int id, ApplicationContext db) =>
{
    if (await db.Messages.FindAsync(id) is Message m)
    {
        db.Messages.Remove(m);
        await db.SaveChangesAsync();
        return Results.Ok(m);
    }

    return Results.NotFound();
});

app.MapPost("/delete_messages", async (int[] id, ApplicationContext db) =>
{
    var atLeastOne = false;
    
    for (int i = 0; i < id.Length; i++)
    {
        if (await db.Messages.FindAsync(id[i]) is Message m)
        {
            db.Messages.Remove(m);
            atLeastOne = true;
        }
    }

    if (atLeastOne)
    {
        await db.SaveChangesAsync();
        return Results.Ok();
    }

    else return Results.NotFound();    
});

app.Run();

